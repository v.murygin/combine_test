//
//  ViewController.swift
//  Combine_test
//
//  Created by Владислав Мурыгин on 19.01.2021.


import UIKit
import Combine


class ObjectWithPublisher {
    @Published var someString: String = ""
}


class ViewController: UIViewController {
    
    var  textFiled: UITextField =  {
        let text = UITextField()
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        text.frame = CGRect(x: 20 , y: height / 3, width: width - 40, height: 40)
        text.placeholder = " Введите текст"
        text.backgroundColor = .white
        text.layer.cornerRadius = 8
        return text
    }()
    
    var  textLabel: UILabel =  {
        let label = UILabel()
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        label.frame = CGRect(x: 20 , y: height / 4, width: width - 40, height: 50)
        label.textAlignment = .center
        label.text = "I'm a label"
        return label
    }()
    
    var  button: UIButton =  {
        let button = UIButton()
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        button.frame = CGRect(x: width / 4 , y: height / 1.5 , width: width / 2, height: 50)
        button.backgroundColor = UIColor.red
        button.setTitle("Click me!", for: .normal)
        button.layer.cornerRadius = 8
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
        return button
    }()
    
    var switchButton: UISwitch = {
        let switchButton = UISwitch()
        let height = UIScreen.main.bounds.height
        let width = UIScreen.main.bounds.width
        switchButton.frame = CGRect(x: width / 2  - 25 , y: height / 2, width: 0, height: 0)
        switchButton.isOn = false
        switchButton.setOn(true, animated: false)
        switchButton.addTarget(self, action: #selector(switchValueDidChange), for: .valueChanged)
        // switchButton.backgroundColor = .white
        return switchButton
    }()
    
    
    
    private var newSubscriber: AnyCancellable?
    private var newSubscriber1: AnyCancellable?
    private var newSubscriber2: AnyCancellable?
    private var newSubscriber3: AnyCancellable?
    private var newSubscriber4: AnyCancellable?
    
    @Published var switchIsOn = false
    @Published var statusButton = false
    
    var obj = ObjectWithPublisher()
    
    
    let subj = PassthroughSubject<String, Never>()
    
    var subscriptions = Set<AnyCancellable>()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        combineTest()
        
        newSubscriber = NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: textFiled)
            .map{($0.object as? UITextField)?.text ?? ""}
            .assign(to: \.text, on: textLabel)
        
        
        newSubscriber2 = NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: textFiled)
            .map{($0.object as? UITextField)?.text ?? ""}
            .map { text in
                if text == "Скрыть кнопку" {
                    
                    return true
                } else {
                    return false
                }
            }
            .assign(to: \.isHidden, on: button)
        
        newSubscriber1 = $switchIsOn.receive(on: DispatchQueue.main)
            .assign(to: \.isHidden, on: button)
        
        newSubscriber3 = $statusButton.receive(on: DispatchQueue.main).assign(to: \.isHidden, on: textFiled)
        
        
        prependPublisherExample()
        prependOutputExample()
        appendOutputExample()
        
    }
    
  

    func prependOutputExample() {
        let stringPublisher = ["World!"].publisher
        
        stringPublisher
            .prepend("Hello")
            .sink(receiveValue: { print($0) })
            .store(in: &subscriptions)
    }
    
    func prependPublisherExample() {
        let subject = PassthroughSubject<String, Never>()
        let stringPublisher = ["Break things!"].publisher
        
        stringPublisher
            .prepend(subject)
            .sink(receiveValue: { print($0) })
            .store(in: &subscriptions)
        
        subject.send("Run code")
        subject.send(completion: .finished)
    }
    
    func appendOutputExample() {
        let stringPublisher = ["1"].publisher
        
        stringPublisher
            .append("2")
            .append("3")
            .append("4")
            .sink(receiveValue: { print($0) })
            .store(in: &subscriptions)
    }
    
    func  combineTest(){
        
//        let pub = Just("Hello")
//        _ = pub.sink {print($0)}
//        
//        obj.$someString.sink{
//            print("\($0)!")
//        }
//        obj.someString = "Hello Combine!"
//
//        subj.eraseToAnyPublisher()
//        subj.send("Hello")
//        let subscriber = subj.sink(receiveValue: { print($0) })
//        subj.send("Combine")
//        subj.send("!")
//       
        newSubscriber4 = NotificationCenter.default
            .publisher(for: UITextField.textDidChangeNotification, object: textFiled)
            .map{($0.object as? UITextField)?.text ?? ""}
            .sink(receiveValue: { (String) in
                print(String)
            })
        
        
    }
    
    func setUI(){
        view.addSubview(switchButton)
        view.addSubview(textFiled)
        view.addSubview(textLabel)
        view.addSubview(button)
        view.backgroundColor = .gray
        
    }
    
    @objc func buttonAction(sender: UIButton!) {
        statusButton = !statusButton
        sender.pulsate()
        print(statusButton)

//        let vc = SecondVC()
//        vc.modalPresentationStyle = .fullScreen
//        self.present(vc, animated: false)

    }
    

    @objc func switchValueDidChange(sender: UISwitch!) {
        switchIsOn = !sender.isOn
    }
    
}

extension UIButton{
    func pulsate() {
        let pulse = CASpringAnimation(keyPath: "transform.scale")
        pulse.duration = 0.4
        pulse.fromValue = 0.95
        pulse.toValue = 1.0
        pulse.autoreverses = true
        pulse.repeatCount = 0.5
        pulse.initialVelocity = 0.5
        pulse.damping = 1.0
        layer.add(pulse, forKey: nil)
    }
}

